import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

// Autotests for https://www.phptravels.net website in JAVA language using Selenium web driver and TestNG
public class SingaporeHotelSearch {

    private WebDriver driver;

    //
    @BeforeTest
    public void openPhptravels(){
        // Test driver set to Google Chrome
        System.setProperty("webdriver.chrome.driver", ".\\chromedriver.exe");
        driver = new ChromeDriver();

        // Preparing the environment for work
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        // Opens phptravels.net
        driver.get("https://www.phptravels.net/");
    }

    // a - In the first input field enter phrase “Singa” and select location “Singapore,Singapore”
    @Test(priority = 1)
    public void selectSingapore()
    {
        // Finds search box
        WebElement searchBox = driver.findElement(By.className("select2-choice"));

        // Click on it and writes "Singa"
        searchBox.click();
        searchBox.sendKeys("Singa");

        // Clicks "Singapore, Singapore"
        driver.findElement(By.xpath("//ul[@class='select2-results']/li[2]/ul[1]/li[1]")).click();
    }

    // b - Select check-in and check-out dates: from 2018 december 1st - to 2018 december 10th
    @Test(priority = 2)
    public void checkInOut()
    {
        // Clicks on Check In
        driver.findElement(By.name("checkin")).click();
        // Selects date
        driver.findElement(By.name("checkin")).sendKeys("01/12/2018");
        //driver.findElement(By.linkText("1")).click();
        // Clicks on Check Out
        driver.findElement(By.name("checkout")).click();
        // Selects date
        driver.findElement(By.name("checkout")).sendKeys("10/12/2018");
        //driver.findElement(By.linkText("10")).click();
    }

    // c - Increase number adults from 2 to 4 and apply search
    @Test(priority = 3)
    public void increaseTravelers()
    {
        // Click on field for traveler number
        driver.findElement(By.id("travellersInput")).click();
        //Increases adult number
        WebElement plusBtn = driver.findElement(By.id("adultPlusBtn"));
        plusBtn.click();
        plusBtn.click();

        // Click search button
        driver.findElement(By.xpath("//form[@name='fCustomHotelSearch']/div[5]/button")).click();

    }

    // d - Open the details of “Rendezvous Hotels”, which appeared in the search result
    @Test(priority = 4)
    public void openRendezvousHotelsDetails ()
    {
        driver.findElement(By.xpath("//a[@href='https://www.phptravels.net/hotels/detail/Singapore/Rendezvous-Hotels']/button")).click();
    }

    // e - In “RELATED HOTELS” section find “Hyatt Regency Perth” hotel and open it.
    @Test(priority = 5)
    public void findHyattRegencyPerth()
    {
        // Scrolls down to find element carousel
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        while(!driver.findElement(By.className("icon-left-open-3")).isDisplayed())
        javascriptExecutor.executeScript("window.scrollBy(0,1000)");

        // Click on left arrow and on the link to "Hyatt Regency Park" hotel
        driver.findElement(By.className("icon-left-open-3")).click();
        driver.findElement(By.xpath("//a[@href='https://www.phptravels.net/hotels/united-arab-emirates/dubai/Hyatt-Regency-Perth']")).click();
    }

    @AfterTest
    public void closeBrowser()
    {
        // Closes the browser and ends work
        driver.quit();
    }
}
